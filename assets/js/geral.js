 

 document.addEventListener('DOMContentLoaded', (e) =>{
 
     $('.summernote').summernote({
        popover: {
            image: [],
            link: [],
            air: []
            },
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']]
        ]
      });
});

function callDeleteItem(selector,id){
    document.getElementById(selector).style.display = `none`;
    $.ajax({
        url: 'cursos/delete/' + id, 
        type: 'DELETE',
        data:id
    });
};

function showname (idseletor,labelseletor) {
    var name = document.getElementById(idseletor); 
    var label = document.getElementsByClassName(labelseletor)[0];
    console.log(label); 
    label.textContent = name.files.item(0).name;

  };
  
   