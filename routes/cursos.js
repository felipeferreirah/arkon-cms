var express = require('express');
var router = express.Router();
var pool = require('../db');
multer = require('multer');
const fs = require('fs');
var results__;
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) { 
    cb(null, file.fieldname + '-' + Date.now()+'.jpg')
  }
})
 
var upload = multer({ storage: storage })
/* GET home page. */
router.get('/', function(req, res, next) {

  pool.getConnection(function(err, connection) {
    // Use the connection
    connection.query('SELECT  * from  cursos ', function (error, results, fields) {
      // And done with the connection.
      connection.release();

      // Handle error after the release.
      if (error) throw error;
      results__= results;
       
        res.render('cursos', {session: req.session, titleController:'Cursos', data: results__ });
    });
  
  
  });
  
});

// ADD NEW USER POST ACTION
router.post('/add',upload.single("newfile"), function(req, res, next){ 
 let body = req.body;
 pool.getConnection(function(err, connection) {
  // Use the connection
  //reading  post data
const name    = body.name;
const descri  = body.descr;
const cat     = body.cat;
let   str     = body.link.replace(/\s/g, '');
const link    = str; 
const type    = body.select; 
const text    = body.text; 
let imgPath   = req.file ? req.file.path : 0 ; 
//save into db start
  connection.query("INSERT INTO cursos  ( name, description, date, category, type, link, text, cover, status) VALUES ( '"+name+"','"+ descri+"','00-00-00','"+ cat+"','"+type+"','"+ link+"','"+ text+"','"+imgPath+"','1') ", function (error, results, fields) {
    // And done with the connection.
    connection.release();
 
  });

  res.redirect('/cursos');
});
 
 console.log(body);

}) 
  
 
router.post('/update',upload.single("fileupdate"), function(req, res, next){ 
  let imgPath   = req.file ? req.file.path : 0 ; 
  const {id_curso,name,descr,cat,link,type,text} = req.body;

  console.log("bateu aqui");
  pool.getConnection(function(err, connection) {
    let queryUpdate; 
    let param ;
 if(imgPath){ 
    queryUpdate  = `UPDATE cursos SET name = ?, description = ?, category = ?, link = ?, text = ?, type = ?, cover = ? WHERE id_curso = ${id_curso}`;
    param        = [name,descr,cat,link,text,type,imgPath];
 }else{
    queryUpdate  = `UPDATE cursos SET name = ?, description = ?, category = ?, link = ?, text = ?, type = ? WHERE id_curso = ${id_curso}`;
    param        = [name,descr,cat,link,text,type];
 }
      
   connection.query(queryUpdate,param, function (error, results, fields) {
    if (error) throw error;
    if (results) console.log(results);
     // And done with the connection.
     connection.release();
 
   });
 
   res.redirect('/cursos');
 });
   
 
 })

 router.delete('/delete/:id', function(req, res, next){ 

  var id_curso = req.params.id; 
  console.log(id_curso); 
  const queryUpdate  = `DELETE FROM cursos WHERE id_curso = ${id_curso}`;

  pool.getConnection(function(err, connection) {
 
   connection.query(queryUpdate, function (error, results, fields) {
    if (error) throw error;
    if (results) console.log(results);
     // And done with the connection.
     connection.release(); 
   });  
 });
   
 
 })
 
module.exports = router;
