var express = require('express');
var router = express.Router();
var pool = require('../db');
var results__;

/* GET home page. */
router.get('/', function(req, res, next) {

  pool.getConnection(function(err, connection) {
    // Use the connection
    connection.query('SELECT  * from  dashboard ', function (error, results, fields) {
      // And done with the connection.
      connection.release();

      // Handle error after the release.
      if (error) throw error;
      results__= results;
      
      console.log(results);
        res.render('index', {session: req.session, titleController:'Principal', data: results__ });
    });
  
  
  });


});
 
 
module.exports = router;
