var express = require('express');
var router = express.Router();
var pool = require('../db');

/* GET users page. */
router.get('/', function(req, res, next) {
  res.render('login', {titleController:'Login', msg: '' });
});

/* GET sair. */
router.get('/sair', function(request, response, next) {
  request.session.destroy();
  response.redirect('/login');
});

/* POST users page. */
router.post('/', function(request, response, next) {
  var username = request.body.username;
  var password = request.body.password; 
	if (username && password) {
    pool.getConnection(function(err, connection) {
      connection.query('SELECT  * from  users  WHERE login = ? AND password = ?', [username, password], function(error, results, fields) {
        if (results.length > 0) {
          request.session.loggedin = true;
          request.session.name = results[0].name;
          request.session.email = results[0].email;
          request.session.username = username; 
          request.session.cookie.expires = new Date(Date.now() + 3600000)
          response.redirect('/home');
        } else {
          response.render('login', {titleController:'Login', msg: 'Usuário e senha não existe!' });
        }			
        response.end();
      });
    });
	} else {
    response.render('login', {titleController:'Login', msg: 'Please enter Username and Password!' });
	  
	}
});



module.exports = router;
