var express = require('express');
var router = express.Router();
var pool = require('../db');

/* GET users page. */
router.get('/', function(req, res, next) {
 
  pool.getConnection(function(err, connection) {
    // Use the connection
    connection.query('SELECT  * from  users ', function (error, results, fields) {
      // And done with the connection.
      connection.release();

      // Handle error after the release.
      if (error) throw error;
      results__= results;  
        res.render('users', {session: req.session, titleController:'Usuários', data: results__ || null });
    });
  
  
  });


});



module.exports = router;
